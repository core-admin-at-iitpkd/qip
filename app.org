#+TITLE: Application for starting a new QIP centre
#+OPTIONS: toc:nil author:nil date:nil
#+LATEX_HEADER: \usepackage{fullpage}
#+LATEX_HEADER: \usepackage{multicol}
#+LATEX_CLASS_OPTIONS: [a4paper,10pt]

* Name of the Institute and Address

  - Name:    :: Indian Institute of Technology Palakkad
  - Address: :: Ahalia Integrated Campus, Kozhippara P. O
  - City:    ::  Palakkad,
  - State:   :: Kerala
  - Pincode: :: 678557

* Contact Details

  - Phone Number: :: 04923 226 300
  - Fax Number:   :: 04923 226 300
  - Email:        :: registrar@iitpkd.ac.in

* Details of the Head of the Institute

  - Name:          :: Sovan Das
  - Designation:   :: Dean of Academics
  - Email:         :: deanacad@iitpkd.ac.in

* Type of the Institute

  #+LATEX: \begin{multicols}{4}
  - [ ] Govt
  - [ ] SPA
  #+LATEX: \columnbreak
  - [ ] Govt. aided
  - [ ] Affiliated
  #+LATEX: \columnbreak
  - [X] IIT
  - [ ] Autonomous
  #+LATEX: \columnbreak
  - [ ] NIT
 #+LATEX: \end{multicols}

* Institute ID:   U-0878  (AISHE code)
* Year of Establishment: 2015 CE
\newpage
* Name of the Courses Offered

  #+ATTR_LATEX: :align |l|l|l|l|l|l| :center nil :environment longtable
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      | Name of the        | Branch                    | Intake |      Year of | NBA         |
  |      | Course             |                           |        | Commencement | Accredition |
  |      |                    |                           |        |              | Status      |
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      |                    |                           |        |              |             |
  | UG   | Bachelor of        | Civil Engg.               |     35 |         2015 |             |
  |      | Technology         | Computer Sci. and Engg.   |     45 |         2015 |             |
  |      | B. Tech            | Electrical Engg.          |     40 |         2015 |             |
  |      |                    | Mechanical Engg.          |     35 |         2015 |             |
  |      |                    |                           |        |              |             |
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      |                    |                           |        |              |             |
  | PG   | Master of          | Computing and Mathematics |     12 |         2020 |             |
  |      | Technology         | Data Science              |     20 |         2020 |             |
  |      | M. Tech            | Geotechnical Engg.        |     15 |         2019 |             |
  |      |                    |                           |        |              |             |
  |      |                    | Manufacturing and         |     15 |         2019 |             |
  |      |                    | Materials Engg.           |        |              |             |
  |      |                    |                           |        |              |             |
  |      |                    | Power Electronics and     |     10 |         2020 |             |
  |      |                    | Power Systems             |        |              |             |
  |      |                    |                           |        |              |             |
  |      |                    | System on Chip Design     |     20 |         2020 |             |
  |      |                    |                           |        |              |             |
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      |                    |                           |        |              |             |
  | PG   | Master of          | Chemistry                 |     20 |         2019 |             |
  |      | Science            | Mathematics               |     20 |         2020 |             |
  |      | M. Sc.             | Physics                   |     20 |         2019 |             |
  |      |                    |                           |        |              |             |
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      |                    |                           |        |              |             |
  | PG   | Master of          | Civil Engg.               |        |         2017 |             |
  |      | Science            | Computer Sci. and Engg.   |        |         2018 |             |
  |      | (by Research), M.S | Electrical Engg.          |        |         2017 |             |
  |      |                    | Mechanical Engg.          |        |         2017 |             |
  |      |                    |                           |        |              |             |
  |------+--------------------+---------------------------+--------+--------------+-------------|
  |      |                    |                           |        |              |             |
  | Ph.D | Doctor of          |                           |        |         2017 |             |
  |      | Philosophy         |                           |        |              |             |
  |      |                    |                           |        |              |             |
  |------+--------------------+---------------------------+--------+--------------+-------------|

\newpage

* Faculty Department/Branch wise

   #+ATTR_LATEX: :align |l|l|l|l|l|l| :center nil :environment longtable
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   | S no | Department                 | Designation        | Qualification | Experince | Research Guide |
   |      |                            |                    |               |   (Years) | Approved by    |
   |      |                            |                    |               |           | University?    |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |    1 | Bio. Sci. And Engg.        | PR - 1             | Ph.D          |        10 | Yes            |
   |      |                            | DBT Ramalingaswami | Ph.D          |         3 | Yes            |
   |      |                            | Fellow - 1         |               |           |                |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    2 | Chemistry                  | AP - 8             | Ph.D          |         3 | Yes            |
   |      |                            | IF - 1             | Ph.D          |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    3 | Civil Engg.                | AP - 14            | Ph.D          |         3 | Yes            |
   |      |                            | Visiting AP - 1    | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    4 | Computer Science And Engg. | ASP - 1            | Ph.D          |         6 | Yes            |
   |      |                            | AP - 11            | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    5 | Electrical Engg.           | PR - 1             | Ph.D          |        10 | Yes            |
   |      |                            | AP - 15            | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    6 | Humanities                 | AP - 4             | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    7 | Mathematics                | PR - 1             | Ph.D          |        10 | Yes            |
   |      |                            | Visiting PR - 1    | Ph.D          |        10 | Yes            |
   |      |                            | AP - 9             | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    8 | Mechanical Engg.           | ASP - 4            | Ph.D          |         6 | Yes            |
   |      |                            | AP - 11            | Ph.D          |         3 | Yes            |
   |      |                            |                    |               |           |                |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |      |                            |                    |               |           |                |
   |    9 | Physics                    | PR - 1             | Ph.D          |        10 | Yes            |
   |      |                            | AP - 9             | Ph.D          |         3 | Yes            |
   |      |                            | IF - 1             | Ph.D          |           | Yes            |
   |------+----------------------------+--------------------+---------------+-----------+----------------|
   |   10 | Sustainability             | AP - 1             | Ph.D          |         3 | Yes            |
   |------+----------------------------+--------------------+---------------+-----------+----------------|

** Legend

   #+ATTR_LATEX: :align |l|l| :center nil
   |-----+---------------------|
   | AP  | Assistant Professor |
   | ASP | Associate Professor |
   | PR  | Professor           |
   | IF  | Inspire Faculty     |
   |-----+---------------------|


* Infrastructure Available

  #+ATTR_LATEX: :align |l|l|l|l| :center nil
  |   | Description                  | Area in $m^2$ |
  |---+------------------------------+---------------|
  | 1 | Academic Building            |          7205 |
  | 2 | Administration               |           917 |
  | 3 | Hostel Facility for Boys     |         16625 |
  | 4 | Hostel Facility for Girls    |          3223 |
  | 5 | Central Institute Facilities |           336 |
  | 6 | Workshops                    |          2771 |
  |---+------------------------------+---------------|

* COMMENT Info from IWD

| Sl No | Description                  | Temporary | Nila | Total in Sq.m. |
|-------+------------------------------+-----------+------+----------------|
|     1 | Academic Building            |      5144 | 2061 |           7205 |
|     2 | Administration               |       457 |  460 |            917 |
|     3 | Hostel Facility-Boys         |      9375 | 7250 |          16625 |
|       | Hostel Facility-Girls        |      1800 | 1423 |           3223 |
|     4 | Central Institute Facilities |       Nil |  336 |            336 |
|     5 | Workshops                    |       Nil | 2771 |           2771 |



* Status AICTE Approval

  IIT Palakkad is declared an /Institute of National Importance/ under
  the Institutes of Technology Act 1961. Hence, AICTE Approval is not
  relevant.

* Details of proposals under QIP

** Ph.D program

  The numbers given are maximum possible candidates present at any
  given time and /not/ the intake per year.

  #+ATTR_LATEX: :align |l|l|l| :center nil :environment longtable
  |--------+-------------------------------------+-----------------------|
  | Sl. no | Name of the PG Dept                 | No. of Seats Proposed |
  |        | considered under QIP                |             under QIP |
  |--------+-------------------------------------+-----------------------|
  |        | Biological Sciences and Engineering |                     1 |
  |        | Chemistry                           |                     3 |
  |        | Physics                             |                     3 |
  |        | Mathematics                         |                     3 |
  |        | Humanities and Social Sciences      |                     1 |
  |        | Civil Engineering                   |                     6 |
  |        | Computer Science and Engineering    |                     6 |
  |        | Data Science                        |                     3 |
  |        | Electrical Engineering              |                     6 |
  |        | Mechanical Engineering              |                     6 |
  |        | ESSENCE                             |                     1 |
  |--------+-------------------------------------+-----------------------|

** MTech Programs

   The numbers given in the table below are the maximum possible
   /intake per year/.

   #+ATTR_LATEX: :align |l|l|l| :center nil :environment longtable
  |--------+-----------------------------------------+-----------------------|
  | Sl. no | Name of the PG Dept                     | No. of Seats Proposed |
  |        | considered under QIP                    |             under QIP |
  |--------+-----------------------------------------+-----------------------|
  |        | Data Science                            |                     1 |
  |        | Geotechnical Engineering                |                     1 |
  |        | Computing and Mathematics               |                     1 |
  |        | Manufacturing and Materials Engineering |                     1 |
  |        | Power Electronics and Power Systems     |                     1 |
  |        | System On Chip Design                   |                     1 |
  |--------+-----------------------------------------+-----------------------|


** TODO COMMENT

   A detailed write up of giving information regarding
   laboratory/library/Infrastructure research facility for each
   department should be provided in separate sheets.

* Housing facilities for QIP scholars

  - Quarters: :: Not available as of now. Hostel accommodation will be
    granted.

  - Hostel:   :: 52

* About the Institute

  Established in 2015, the Indian Institute of Technology Palakkad is
  dedicated to creating an environment that enables students and
  faculty to engage in the pursuit of knowledge, to dream, think and
  innovate thereby becoming a change agent for a better world.
  Beginning with a humble student strength of 120 way back in July
  2015, IIT Palakkad has now groomed itself into an establishment
  which is 1000 students strong with the best of manpower in its key
  positions. IIT Palakkad targets to become a multi-disciplinary
  institution with a population of 5000 students in 10 years. IIT
  Palakkad recognizes collective growth, in collaboration with
  industry and other academic institutions, as the need of the time
  and emphasizes blue-sky research and directed research as two
  essential pillars of technology development. IIT Palakkad embraces a
  vision to be a leader in cross-disciplinary inquiries which is
  embodied in the tagline of the institute "Nurturing minds for a
  better world".

  Celebrated as a major granary of Kerala, Palakkad is the gateway to
  the State from the North in the form of the nearly 40-kilometre
  break in the Western Ghats, called the Palakkad Gap. The place is
  known for its rich traditions, great historical events and
  personalities connected with it, and its sylvan surroundings,
  especially the Silent Valley rain forests and the famed palmyra
  trees. We currently operate from two campuses: the temporary campus,
  in the premises of the Ahalia Integrated campus, Kozhippara, and a
  transit campus nestled in a corner of the permanent campus in
  Kanjikode. The academic space named Samgatha and the laboratory
  complex named Manogatha have been functional since 2019. The
  students are accommodated in the three newly built hostels ---
  Bageshri, Brindavani and Tilang. Our campus has excellent
  infrastructure in place, which comprises an auditorium, multimedia
  enabled small and large classrooms, student laboratories, library,
  canteen, and fully WiFi enabled hostels with mobility in Internet
  access. Our permanent campus is fast coming up on a picturesque
  500-acre plot at the foot of the Western Ghats and is situated
  within a few kilometres from the Palakkad Railway Junction and about
  60 kilometres from the Coimbatore International Airport. With our
  excellent faculty, students who excel both in academics and in
  extra-curricular activities, dedicated staff members and
  state-of-the-art research facilities, IIT Palakkad has emerged out
  of its infancy and entered an exciting chapter of its life.

** COMMENT What to write

   A brief write up (500 words) by the Institute (mentioning the
   *strengths* and *weaknesses*) that why the institute to be
   considered for the QIP centre.

* Declaration by the Head of the Institute


  I _Sovan Das_ confirm that the above information is true to
  the best of my knowledge.

  #+LATEX: \begin{multicols}{2}
  - Date  ::
  - Place :: Palakkad
  #+LATEX:\columnbreak
  - Name  :: Sovan Das
  - Designation :: Dean Academics
  #+LATEX: \end{multicols}

  \begin{flushright}
   (Head of the Institution)
  \end{flushright}
