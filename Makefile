GENERATED=pdf fdb_latexmk fls log aux out tex toc html
.PHONY: clean all tarball
%.tex : %.org
	emacs -Q $< --batch -f org-latex-export-to-latex --kill
%.pdf : %.tex
	latexmk -pdf $<

.DEFAULT_GOAL = all
all: app.pdf

clean:
	rm -f $(addprefix app., ${GENERATED})

policy.pdf: policy.tex


tarball:
	git archive HEAD --format tgz --prefix automation-`git rev-parse HEAD`/ -o automation-`git rev-parse --short HEAD`.tgz
