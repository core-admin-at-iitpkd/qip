with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    emacs
    gnumake
    texlive.combined.scheme-full
  ];
}
